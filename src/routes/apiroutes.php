<?php
$app->group('/api', function () use ($app) {

	$app->get('/users[?page={page:[0-9]+}&items_per_page={limit:[0-9]+}]', 'UsersController:index');
	$app->get('/users/{user_id}', 'UsersController:listOne');
	$app->post('/users', 'UsersController:create');
	$app->put('/users/update', 'UsersController:update');
	$app->post('/users/confirm', 'UsersController:confirm');
	$app->post('/users/search', 'UsersController:search');
	
	$app->post('/administrators', 'AdministratorsController:create');
	$app->post('/administrators/confirm', 'AdministratorsController:confirm');
	$app->get('/administrators', 'AdministratorsController:index');
	$app->get('/administrator/{administrator_id}', 'AdministratorsController:listOne');

	$app->post('/authentication/request', 'AuthenticationController:generateLink');
	$app->post('/authentication/reset', 'AuthenticationController:resetPassword');
	$app->post('/authentication/admin', 'AuthenticationController:authenticateAdmin');
	$app->post('/authentication/applicant', 'AuthenticationController:authenticateApplicant');

	$app->get('/applications/apply/{job_id}', 'ApplicationsController:create');
	$app->get('/applications', 'ApplicationsController:index');
	$app->get('/applications/applicant', 'ApplicationsController:indexForApplicant');
	$app->get('/applications/vacancy/{job_id}', 'ApplicationsController:indexForVacancy');
	$app->get('/application/{application_id}', 'ApplicationsController:listOne');
	$app->delete('/applications', 'ApplicationsController:delete');

	$app->post('/companies', 'CompaniesController:create');
	$app->get('/companies', 'CompaniesController:index');
	$app->get('/companies/search', 'CompaniesController:search');
	$app->get('/companies/{company_id}', 'CompaniesController:listOne');
	$app->get('/company/confirm[?company={company_id}&conf={confirmation_code}]', 'CompaniesController:confirm');
	$app->delete('/companies/delete/{company_id}', 'CompaniesController:delete');

	$app->post('/industries', 'IndustriesController:create');
	$app->get('/industries', 'IndustriesController:index');
	$app->get('/industries/{industry_id}', 'IndustriesController:listOne');
	
	$app->post('/vacancies', 'VacanciesController:create');
	$app->get('/vacancies', 'VacanciesController:index');
	$app->get('/vacancy/{vacancy_id}', 'VacanciesController:listOne');
	$app->get('/vacancies/applicant', 'VacanciesController:indexForApplicant');
	$app->get('/vacancies/delete/{vacancy_id}', 'VacanciesController:delete');
	$app->get('/vacancies/search/{search_term}', 'VacanciesController:search');

	$app->get('/stats/admin', 'DashboardStatsController:adminStats');
	$app->get('/stats/applicant', 'DashboardStatsController:applicantStats');

	$app->post('/documents/upload', 'FilesController:uploadDoc');
	$app->get('/documents/{applicant_id}', 'FilesController:getApplicantDocs');
	$app->post('/images/upload', 'FilesController:uploadImage');

});