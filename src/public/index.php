<?php
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Headers: Content-Type');
header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

// header('Access-Control-Allow-Origin: *');
// header('Access-Control-Allow-Methods: GET, POST'); 

require '../../vendor/autoload.php';

$config['displayErrorDetails'] = true;
$config['addContentLengthHeader'] = false;
$app = new \Slim\App(["settings" => $config]);

class CorsMiddleware 
// extends \Slim\Middleware
{
    public function call()
    {
        //The Slim application
        $app = $this->app;

        //Response
        $response = $app->response;
        $response->headers->set('Access-Control-Allow-Origin', '*');
        $response->headers->set('Access-Control-Allow-Credentials', 'true');
        $response->headers->set('Access-Control-Allow-Methods', 'GET,POST,PUT,DELETE,OPTIONS');
        $response->headers->set('Access-Control-Allow-Headers', 'Origin, Content-Type, Accept, Authorization, user-id, admin-id');

        $this->next->call();
    }
}

$container = $app->getContainer();

use RedBeanPHP\R;

// R::setup('mysql:host=localhost;dbname=jestra_api', 'root', '');
R::setup('mysql:host=localhost;dbname=rajapi', 'root', '');

R::setAutoResolve( TRUE );

require '../config/logger.php';
require '../config/database.php';
require '../config/dependencies.php';
require '../routes/apiroutes.php'; 

$app->run();