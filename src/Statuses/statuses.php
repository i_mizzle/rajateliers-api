<?php

    namespace App\Statuses;

    class Statuses {

        public function getstatus($code, $objectresponse=null) {
            $codearr = 
            [
                 5000 => "account creation successful",
                 5001 => "account creation failed",
                 5002 => "account not found",
                 5003 => "account already exists",
                 5004 => "vacancy does not exist",
                 5005 => "bad request/field sent or one of the fields empty",
                 5006 => "deletion successful",
                 5007 => "applicant has no applications in progress",
                 5008 => "This account has been confirmed",
                 5009 => "applicant/admin id required",
                 6000 => "operation successful",
                 6001 => "operation failed",
            ];

            $status = $codearr[$code];
            $statusHandler = [ 'status_code' => $code,'status' => $status, 'data' => $objectresponse];
            return $statusHandler;
        }

        public function allAccountResponse($code, $objectresponse = null, $message = null,$page=0, $limit=0,$total_page=0) {
            $codearr = [
                6000 => true,
                6001 => false,

            ];

            $response = $codearr[$code];
            if($code==6000){
                $statusHandler = ['success' => $response,'page'=>$page,'items_per_page'=>$limit,'total_page'=>$total_page,'data' => $objectresponse];
                
            }else if($code==6001){
                $statusHandler = ['success' => $response,'message' => $message,'code'=>$code];
            }else{
                $statusHandler = ['success' => $response,'message' => $message,'code'=>$code];
            }
            return $statusHandler;
        }
        
        public function oneAccountResponse($code, $objectresponse = null, $message = null) {
            $codearr = [
                6000 => true,
                6001 => false,
                
            ];
            $response = $codearr[$code];
            if($code==6000){
                $statusHandler = ['success' => $response,'message' => $message,'code'=>$code];
            }else{
                $statusHandler = ['success' => $response,'message' => $message,'code'=>$code];
            }
            return $statusHandler;
        }

   }