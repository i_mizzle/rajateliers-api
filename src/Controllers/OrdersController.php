<?php
namespace App\Controllers;

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use App\Statuses\Statuses;
use App\Models\Validator;
use App\Models\ApplicationsModel as Applications;
use App\Models\AuthenticationModel as Authentication;

class ApplicationsController
{
    protected $container;
    
    public function __construct($container)
    {
        $this->container = $container;
    }

    public function create(Request $request, Response $response)
    {        
        $job_id = $request->getAttribute('job_id');
        $authenticate = new Authentication();
        $headers = $request->getHeaders();
        
        $token = $headers['HTTP_AUTHORIZATION'][0];
        $user_id = $headers['HTTP_USER_ID'][0];
        
        $validate = new Validator();
        $auth_errors = $validate->authToken($token, $user_id);
        if (!empty($auth_errors['data'])){
            return $response->withJson($auth_errors)
                ->withHeader('Content-type', 'application/json')
                ->withStatus(401);
        }

        $errors = $validate->applicationPayload($job_id, $user_id);

        if (!empty($errors['data'])){
            return $response->withJson($errors)
                ->withHeader('Content-type', 'application/json')
                ->withStatus(400);
        }
        
        $applications = new Applications();
        $data = $applications->createNewApplication($user_id, $job_id);

        return $response->withJson($data)
            ->withHeader('Content-type', 'application/json')
            ->withStatus(201);
    }

    public function index(Request $request, Response $response)
    {
        $paramValue = $request->getQueryParams();
        $authenticate = new Authentication();
        $headers = $request->getHeaders();
        
        $token = $headers['HTTP_AUTHORIZATION'][0];
        $user_id = $headers['HTTP_ADMIN_ID'][0];
        
        $validate = new Validator();
        $auth_errors = $validate->authToken($token, $user_id);
        if (!empty($auth_errors['data'])){
            return $response->withJson($auth_errors)
                ->withHeader('Content-type', 'application/json')
                ->withStatus(401);
        }
        
        if (!empty($paramValue['page']))
        {
            $page = $paramValue['page'];
        }
        else
        {
            $page = 1;
        }

        if (!empty($paramValue['items_per_page']))
        {
            $limit =  $paramValue['items_per_page'];
        }
        else
        {
            $limit = 10;
        }

        $applications = new Applications();
        $data = $applications->listAllApplications((int)$page, (int)$limit);

        return $response->withJson($data)
            ->withHeader('Content-type', 'application/json')
            ->withStatus(200);
    }

    public function indexForVacancy(Request $request, Response $response)
    {
        $job_id = $request->getAttribute('job_id');
        $paramValue = $request->getQueryParams();
        $authenticate = new Authentication();
        $headers = $request->getHeaders();
        
        $token = $headers['HTTP_AUTHORIZATION'][0];
        $user_id = $headers['HTTP_ADMIN_ID'][0];
        
        $validate = new Validator();
        $auth_errors = $validate->authToken($token, $user_id);
        if (!empty($auth_errors['data'])){
            return $response->withJson($auth_errors)
                ->withHeader('Content-type', 'application/json')
                ->withStatus(401);
        }

        if (!empty($paramValue['page']))
        {
            $page = $paramValue['page'];
        }
        else
        {
            $page = 1;
        }

        if (!empty($paramValue['items_per_page']))
        {
            $limit =  $paramValue['items_per_page'];
        }
        else
        {
            $limit = 10;
        }

        $applications = new Applications();
        $data = $applications->getVacancyApplications((int)$page, (int)$limit, $job_id);

        return $response->withJson($data)
            ->withHeader('Content-type', 'application/json')
            ->withStatus(200);
    }

    public function indexForApplicant(Request $request, Response $response)
    {
        $paramValue = $request->getQueryParams();
        $authenticate = new Authentication();
        $headers = $request->getHeaders();
        
        $token = $headers['HTTP_AUTHORIZATION'][0];
        $user_id = $headers['HTTP_USER_ID'][0];
        
        $validate = new Validator();
        $auth_errors = $validate->authToken($token, $user_id);
        if (!empty($auth_errors['data'])){
            return $response->withJson($auth_errors)
                ->withHeader('Content-type', 'application/json')
                ->withStatus(401);
        }

        if (!empty($paramValue['page']))
        {
            $page = $paramValue['page'];
        }
        else
        {
            $page = 1;
        }

        if (!empty($paramValue['items_per_page']))
        {
            $limit =  $paramValue['items_per_page'];
        }
        else
        {
            $limit = 10;
        }

        $applications = new Applications();
        $data = $applications->getApplicantApplications((int)$page, (int)$limit, $user_id);

        return $response->withJson($data)
            ->withHeader('Content-type', 'application/json')
            ->withStatus(200);
    }

    public function listOne(Request $request, Response $response)
    {
        $application_id = $request->getAttribute('application_id');

        $applications = new Applications();
        $data = $applications->listOneApplication($application_id);

        return $response->withJson($data)
            ->withHeader('Content-type', 'application/json')
            ->withStatus(200);
    }
} 