<?php
namespace App\Controllers;

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use App\Statuses\Statuses;
use App\Models\Validator;
use App\Models\ApplicantsModel as Applicants;
use App\Models\AuthenticationModel as Authentication;
use App\Models\DashboardStatsModel as Stats;


class DashboardStatsController 
{
    protected $container;
    
    public function __construct($container)
    {
        $this->container = $container;
    }

    public function applicantStats(Request $request, Response $response)
    {
        $authenticate = new Authentication();
        $headers = $request->getHeaders();
        
        $token = $headers['HTTP_AUTHORIZATION'][0];
        $user_id = $headers['HTTP_USER_ID'][0];
        
        $validate = new Validator();
        $auth_errors = $validate->authToken($token, $user_id);
        
        if (!empty($auth_errors['data'])){
            return $response->withJson($auth_errors)
                ->withHeader('Content-type', 'application/json')
                ->withStatus(401);
        }

        $body = $request->getBody();
        $input = json_decode($body, true);

        $stats = new Stats;
        $data = $stats->getApplicantDashboardStats($user_id);
        
        return $response->withJson($data)
            ->withHeader('Content-type', 'application/json')
            ->withStatus(200);

    }
}
