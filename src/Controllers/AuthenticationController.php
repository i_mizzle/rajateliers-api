<?php
namespace App\Controllers;

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use App\Statuses\Statuses;
use App\Models\Validator;
use App\Models\AuthenticationModel as Authentication;

class AuthenticationController
{
    protected $container;
    
    public function __construct($container)
    {
        $this->container = $container;
    }

    public function authenticateApplicant(Request $request, Response $response)
    {
        $body = $request->getBody();
        $input = json_decode($body, true);

        $validate = new Validator();
        $errors = $validate->applicantAuthenticationPayload($input);

        if (!empty($errors['data'])){
            return $response->withJson($errors)
                ->withHeader('Content-type', 'application/json')
                ->withStatus(400);
        }
        
        $auth = new Authentication;
        $data = $auth->authenticateApplicant($input);
        
        return $response->withJson($data)
            ->withHeader('Content-type', 'application/json')
            ->withStatus(200);
    }
        
    public function authenticateAdmin(Request $request, Response $response)
    {
        $body = $request->getBody();
        $input = json_decode($body, true);

        $validate = new Validator();
        $errors = $validate->adminAuthenticationPayload($input);

        if (!empty($errors['data'])){
            return $response->withJson($errors)
                ->withHeader('Content-type', 'application/json')
                ->withStatus(400);
        }
        
        $auth = new Authentication;
        $data = $auth->authenticateAdmin($input);
        
        return $response->withJson($data)
            ->withHeader('Content-type', 'application/json')
            ->withStatus(200);
    }

    public function generateLink(Request $request, Response $response)
    {
        # code...
    }

    public function resetPassword(Request $request, Response $response)
    {
        # code...
    }

}
