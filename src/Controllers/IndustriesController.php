<?php
namespace App\Controllers;

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use App\statuses;
Use App\Models\Mailer;
use App\Models\Validator;
use App\Models\IndustriesModel as Industries;
use App\Models\AuthenticationModel as Authentication;

class IndustriesController
{
    protected $container;
    
    public function __construct($container)
    {
        $this->container = $container;
    }

    public function create(Request $request, Response $response)
    {        
        $authenticate = new Authentication();
        $headers = $request->getHeaders();
        
        $token = $headers['HTTP_AUTHORIZATION'][0];
        $user_id = $headers['HTTP_ADMIN_ID'][0];
        
        $validate = new Validator();
        $auth_errors = $validate->authToken($token, $user_id);
        
        if (!empty($auth_errors['data'])){
            return $response->withJson($auth_errors)
                ->withHeader('Content-type', 'application/json')
                ->withStatus(401);
        }
        
        $body = $request->getBody();
        $input = json_decode($body, true);

        $errors = $validate->industriesPayload($input);

        if (!empty($errors['data'])){
            return $response->withJson($errors)
                ->withHeader('Content-type', 'application/json')
                ->withStatus(400);
        }
        
        $companies = new Industries;
        $data = $companies->createNewIndustry($input);

        return $response->withJson($data)
            ->withHeader('Content-type', 'application/json')
            ->withStatus(201);
    }

    public function index(Request $request, Response $response)
    {
 
        $paramValue = $request->getQueryParams();

        if (!empty($paramValue['page']))
        {
            $page = $paramValue['page'];
        }
        else
        {
            $page = 1;
        }

        if (!empty($paramValue['items_per_page']))
        {
            $limit =  $paramValue['items_per_page'];
        }
        else
        {
            $limit = 10;
        }

        $industries = new Industries();
        $data = $industries->listAllIndustries((int)$page, (int)$limit);

        return $response->withJson($data)
            ->withHeader('Content-type', 'application/json')
            ->withStatus(200);
    }

    public function listOne(Request $request, Response $response)
    {

        $industry_id = $request->getAttribute('industry_id');

        $industries = new Industries();
        $data = $industries->listOneIndustry($industry_id);

        return $response->withJson($data)
            ->withHeader('Content-type', 'application/json')
            ->withStatus(200);
    }
}