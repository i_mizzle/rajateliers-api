<?php
namespace App\Controllers;

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use App\statuses;
Use App\Models\Mailer;
use App\Models\Validator;
use App\Models\CompaniesModel as Companies;
use App\Models\AuthenticationModel as Authentication;

class CompaniesController
{
    protected $container;
    
    public function __construct($container)
    {
        $this->container = $container;
    }

    public function create(Request $request, Response $response)
    {        
        $authenticate = new Authentication();
        $headers = $request->getHeaders();
        
        $token = $headers['HTTP_AUTHORIZATION'][0];
        $user_id = $headers['HTTP_ADMIN_ID'][0];
        
        $validate = new Validator();
        $auth_errors = $validate->authToken($token, $user_id);
        
        if (!empty($auth_errors['data'])){
            return $response->withJson($auth_errors)
                ->withHeader('Content-type', 'application/json')
                ->withStatus(401);
        }
        
        $body = $request->getBody();
        $input = json_decode($body, true);

        $errors = $validate->companiesPayload($input);

        if (!empty($errors['data'])){
            return $response->withJson($errors)
                ->withHeader('Content-type', 'application/json')
                ->withStatus(400);
        }
        
        $companies = new Companies;
        $data = $companies->createNewCompany($input);

        return $response->withJson($data)
            ->withHeader('Content-type', 'application/json')
            ->withStatus(201);
    }

    public function index(Request $request, Response $response)
    {
        $authenticate = new Authentication();
        $headers = $request->getHeaders();
        
        $token = $headers['HTTP_AUTHORIZATION'][0];
        $user_id = $headers['HTTP_ADMIN_ID'][0];
        
        $validate = new Validator();
        $auth_errors = $validate->authToken($token, $user_id);
        
        if (!empty($auth_errors['data'])){
            return $response->withJson($auth_errors)
                ->withHeader('Content-type', 'application/json')
                ->withStatus(401);
        }

        $paramValue = $request->getQueryParams();

        if (!empty($paramValue['page']))
        {
            $page = $paramValue['page'];
        }
        else
        {
            $page = 1;
        }

        if (!empty($paramValue['items_per_page']))
        {
            $limit =  $paramValue['items_per_page'];
        }
        else
        {
            $limit = 10;
        }

        $companies = new Companies();
        $data = $companies->listAllCompanies((int)$page, (int)$limit);

        return $response->withJson($data)
            ->withHeader('Content-type', 'application/json')
            ->withStatus(200);
    }

    public function listOne(Request $request, Response $response)
    {

        $company_id = $request->getAttribute('company_id');

        $companies = new Companies();
        $data = $companies->listOneCompany($company_id);

        return $response->withJson($data)
            ->withHeader('Content-type', 'application/json')
            ->withStatus(200);
    }

    public function search(Request $request, Response $response)
    {
        $body = $request->getBody();
        $input = json_decode($body, true);

        $companies = new Companies();
        $data = $companies->searchForComanies($input);

        return $response->withJson($data)
            ->withHeader('Content-type', 'application/json')
            ->withStatus(200);
    }

    public function confirm(Request $request, Response $response)
    { 
        $paramValue = $request->getQueryParams();
        
        $company_id = $paramValue['company'];
        $confirmation_code = $paramValue['conf'];

        $company = new Companies();
        $data = $company->confirmCompanyAccount($company_id, $confirmation_code);

        return $response->withJson($data)
            ->withHeader('Content-type', 'application/json')
            ->withStatus(200);
        
    }

}