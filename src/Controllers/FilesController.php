<?php
namespace App\Controllers;

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use Slim\Http\UploadedFile;
use App\statuses;
Use App\Models\Mailer;
use App\Models\Validator;
use App\Models\FilesModel as Files;
use App\Models\AuthenticationModel as Authentication;

class FilesController
{
    protected $container;
    
    public function __construct($container)
    {
        $this->container = $container;
    }

    public function uploadImage(Request $request, Response $response)
    {    

        $authenticate = new Authentication();
        $headers = $request->getHeaders();
        
        $token = $headers['HTTP_AUTHORIZATION'][0];
        $user_id = $headers['HTTP_USER_ID'][0];
        
        $validate = new Validator();
        $auth_errors = $validate->authToken($token, $user_id);
        
        if (!empty($auth_errors['data'])){
                return $response->withJson($auth_errors)
                ->withHeader('Content-type', 'application/json')
                ->withStatus(401);
        }
        
        $files = $request->getUploadedFiles();
        if (empty($files['photo'])) {
            return $response->withJson('Expected a new file')
                    ->withHeader('Content-type', 'application/json')
                    ->withStatus(400);
        }
        else{
            
            $newfile = $files['photo'];            
            $extension = pathinfo($newfile->getClientFilename(), PATHINFO_EXTENSION);
            
            if($extension !== "png" && $extension !== "jpg" && $extension !== "jpeg")
            {
                return $response->withJson('Sorry, file must be .png, .jpg or .jpeg')
                        ->withHeader('Content-type', 'application/json')
                        ->withStatus(400);
            }

            $basename = bin2hex($user_id); 
            $uploadfilename = sprintf('%s.%0.8s', $basename, $extension);

            $newfile->moveTo("../uploads/$uploadfilename");
            $newfilepath = "http://localhost/jestrasolutions/src/uploads/".$uploadfilename;
            
            $files = new Files;
            $data = $files->saveImagePath($user_id, $newfilepath);
     
            return $response->withJson($data)
                ->withHeader('Content-type', 'application/json')
                ->withStatus(201);
        }
    }

    public function uploadDoc(Request $request, Response $response)
    {    
        
        $authenticate = new Authentication();
        $headers = $request->getHeaders();
        
        $token = $headers['HTTP_AUTHORIZATION'][0];
        $user_id = $headers['HTTP_USER_ID'][0];
        
        $validate = new Validator();
        $auth_errors = $validate->authToken($token, $user_id);
        
        if (!empty($auth_errors['data'])){
            return $response->withJson($auth_errors)
                ->withHeader('Content-type', 'application/json')
                ->withStatus(401);
        }
        
        $doc_type = $_POST['type'];
        $doc_name = $_POST['name'];

        $files = $request->getUploadedFiles();
        
        if (empty($files['document'])) {
            return $response->withJson('Expected a new file')
            ->withHeader('Content-type', 'application/json')
                    ->withStatus(400);
        }
        else{
            
            $newfile = $files['document'];            
            $extension = pathinfo($newfile->getClientFilename(), PATHINFO_EXTENSION);
            
            if($extension !== "pdf" && $extension !== "doc" && $extension !== "docx" && $extension !== "png" && $extension !== "jpg" && $extension !== "jpeg")
            {
                return $response->withJson('Sorry, file must be .doc, .jpg, .docx, .doc, .jpg or .jpeg format')
                        ->withHeader('Content-type', 'application/json')
                        ->withStatus(400);
            }

            $basename = bin2hex($user_id); 
            $uploadfilename = sprintf('%s.%0.8s', $doc_type.$basename, $extension);
            
            $newfile->moveTo("../uploads/$uploadfilename");
            $newfilepath = "http://localhost/jestrasolutions/src/uploads/".$uploadfilename;
            // $newfilepath = "http://api.jestrasolutionstions.com/uploads/".$uploadfilename;
            
            $files = new Files;
            $data = $files->saveDocumentPath($user_id, $newfilepath, $extension, $doc_type, $doc_name);
            
            return $response->withJson($data)
                ->withHeader('Content-type', 'application/json')
                ->withStatus(201);
        }
    }

    public function getApplicantDocs(Request $request, Response $response)
    {
        $applicant_id = $request->getAttribute('applicant_id');
        
        $files = new Files();
        $data = $files->listAllApplicantDocuments($applicant_id);

        return $response->withJson($data)
            ->withHeader('Content-type', 'application/json')
            ->withStatus(200);
    }
}
