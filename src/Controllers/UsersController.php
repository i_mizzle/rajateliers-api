<?php
namespace App\Controllers;

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use App\Statuses\Statuses;
use App\Models\Validator;
use App\Models\UsersModel as Users;
use App\Models\AuthenticationModel as Authentication;

class UsersController
{
    protected $container;
    
    public function __construct($container)
    {
        $this->container = $container;
    }

    public function create(Request $request, Response $response)
    {        
        
        $body = $request->getBody();
        $input = json_decode($body, true);

        $validate = new Validator();
        $errors = $validate->usersPayload($input);

        if (!empty($errors['data'])){
            return $response->withJson($errors)
                ->withHeader('Content-type', 'application/json')
                ->withStatus(400);
        }
        
        $users = new Users;
        $data = $users->createNewUser($input);

        return $response->withJson($data)
            ->withHeader('Content-type', 'application/json')
            ->withStatus(201);
    }

    public function confirm(Request $request, Response $response)
    {
        $body = $request->getBody();
        $input = json_decode($body, true);

        $validate = new Validator();
        $errors = $validate->confirmationPayload($input);

        if (!empty($errors['data'])){
            return $response->withJson($errors)
                ->withHeader('Content-type', 'application/json')
                ->withStatus(400);
        }

        $applicants = new Applicants;
        $data = $applicants->confirmApplicant($input);

        return $response->withJson($data)
            ->withHeader('Content-type', 'application/json')
            ->withStatus(201);
    }

    public function index(Request $request, Response $response)
    {
        $paramValue = $request->getQueryParams();

        if (!empty($paramValue['page']))
        {
            $page = $paramValue['page'];
        }
        else
        {
            $page = 1;
        }

        if (!empty($paramValue['items_per_page']))
        {
            $limit =  $paramValue['items_per_page'];
        }
        else
        {
            $limit = 10;
        }

        $users = new Users();
        $data = $users->listAllUsers((int)$page, (int)$limit);

        return $response->withJson($data)
            ->withHeader('Content-type', 'application/json')
            ->withStatus(200);
    }

    public function listOne(Request $request, Response $response)
    {
        $user_id = $request->getAttribute('user_id');

        $users = new Users();
        $data = $users->listOneUser($user_id);

        return $response->withJson($data)
            ->withHeader('Content-type', 'application/json')
            ->withStatus(200);
    }

    public function update(Request $request, Response $response)
    {
        $authenticate = new Authentication();
        $headers = $request->getHeaders();
        
        $token = $headers['HTTP_AUTHORIZATION'][0];
        $user_id = $headers['HTTP_USER_ID'][0];
        
        $validate = new Validator();
        $auth_errors = $validate->authToken($token, $user_id);
        
        if (!empty($auth_errors['data'])){
            return $response->withJson($auth_errors)
                ->withHeader('Content-type', 'application/json')
                ->withStatus(401);
        }

        $body = $request->getBody();
        $input = json_decode($body, true);

        $applicants = new Applicants;
        $data = $applicants->updateApplicantProfile($input, $user_id);
        
        return $response->withJson($data)
            ->withHeader('Content-type', 'application/json')
            ->withStatus(200);
                    
    }
}