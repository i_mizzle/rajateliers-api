<?php

$container['ApplicationsController'] = function ($container)
	{
		return new \App\controllers\ApplicationsController($container);
	};

$container['UsersController'] = function ($container)
	{
		return new \App\Controllers\UsersController($container);
	};
	
$container['CompaniesController'] = function ($container)
	{
		return new \App\Controllers\CompaniesController($container);
	};

$container['VacanciesController'] = function ($container)
	{
		return new \App\Controllers\VacanciesController($container);
	};

$container['AuthenticationController'] = function ($container)
	{
		return new \App\Controllers\AuthenticationController($container);
	};

$container['AdministratorsController'] = function ($container)
	{
		return new \App\Controllers\AdministratorsController($container);
	};

$container['IndustriesController'] = function ($container)
	{
		return new \App\Controllers\IndustriesController($container);
	};

$container['DashboardStatsController'] = function ($container)
	{
		return new \App\Controllers\DashboardStatsController($container);
	};

$container['FilesController'] = function ($container)
	{
		return new \App\Controllers\FilesController($container);
	};

$container['ImagesController'] = function ($container)
	{
		return new \App\Controllers\ImagesController($container);
	};

?>