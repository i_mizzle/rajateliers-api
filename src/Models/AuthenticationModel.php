<?php
namespace App\Models;

use App\Statuses\Statuses;
use App\Models\Validator;
use App\Models\Mailer;
use RedBeanPHP\R as R;
use \Firebase\JWT\JWT;
use \Tuupola\Base62;

class AuthenticationModel
{

    private function generateAuthToken()
    {
        $token = bin2hex(openssl_random_pseudo_bytes(64));
        return $token;
    }

    private function generatePasswordResetCode()
    {
        # code...
    }

    private function storeToken($user_id, $auth_token)
    {
        $datenow = date("d-m-Y H:i:s");
        $token_expiry = date("Y-m-d H:i:s", strtotime($datenow)+86400);
    
        $token = R::dispense('authenticationtokens');
        $token->id = "";
        $token->user_id = $user_id;
        $token->token = $auth_token;
        $token->date_generated = R::isoDateTime();
        $token->expiry = $token_expiry;

        R::store($token);

        return $token_expiry;
    }
    
    public function authenticateApplicant($input)
    {

        $applicant = R::findOne('applicants', "email=?", [$input['email']]);        
        $applicant_id = $applicant['applicant_id'];

        $password_check = R::findOne('applicantlogin', 'applicant_id=?', [$applicant_id]);
        
        if (password_verify($input['password'], $password_check['password']))
        {

            $auth_token = $this->generateAuthToken();            
            $token_expiry = $this->storeToken($applicant_id, $auth_token);

            $objectresponse = [
                "user_id" => $applicant_id,
                "token" => $auth_token,
                "expiry" => $token_expiry
            ];

            $status_code = 6000;
            $resultHandler = (new Statuses)->getstatus($status_code, $objectresponse);
            return $resultHandler;        
        }
        else
        {
            $objectresponse = "Sorry, the password provided is wrong";
            $status_code = 6001;
            $resultHandler = (new Statuses)->getstatus($status_code, $objectresponse);
            return $resultHandler;        
        }
    }
    
    public function authenticateAdmin($input)
    {
        $administrator = R::findOne('administrators', "email=?", [$input['email']]);        
        $administrator_id = $administrator['admin_id'];
    
        $password_check = R::findOne('adminlogin', 'admin_id=?', [$administrator_id]);

        // print_r($password_check);
        
        if (password_verify($input['password'], $password_check['password']))
        {
    
            $auth_token = $this->generateAuthToken();            
            $token_expiry = $this->storeToken($administrator_id, $auth_token);
    
            $objectresponse = [
                "user_id" => $administrator_id,
                "token" => $auth_token,
                "expiry" => $token_expiry
            ];
    
            $status_code = 6000;
            $resultHandler = (new Statuses)->getstatus($status_code, $objectresponse);
            return $resultHandler;        
        }
        else
        {
            $objectresponse = "Sorry, the password provided is wrong";
            $status_code = 6001;
            $resultHandler = (new Statuses)->getstatus($status_code, $objectresponse);
            return $resultHandler;        
        }
    }

    public function requestPasswordReset($email)
    {
        # code...
    }

}