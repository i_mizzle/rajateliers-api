<?php
namespace App\Models;

class Mailer 
{
    public function sendEmail($subject, $message, $email_address)
    {

	$headers = "From: admin@realgamechangers.org";
	$headers .= "MIME-Version: 1.0\r\n";
	$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

	mail( $email_address, $subject, $message, $headers );

	return true;

    }
    
}
