<?php
namespace App\Models;

use App\Statuses\Statuses;
use App\Models\Validator;
use App\Models\Mailer;
use RedBeanPHP\R as R;

class IndustriesModel
{
    private function generateIndustryID($industryindex, $industryname)
    {
        $index = str_pad($industryindex, 2, '0', STR_PAD_LEFT);
        $name = strtoupper($industryname); 
        $industry_id = "IND".substr($name,0,2).$index;

        return $industry_id;
    }

    public function createNewIndustry($inputdata)
    {
      $industry = R::dispense('industries');

      if($industry){
            $industry->id = "";
            $industry->industry_name = $inputdata['industry_name'];
            $industry->description = $inputdata['description'];
            $industry->date_added = R::isoDateTime();

            $storeindustry = R::store($industry);

            if ($storeindustry) {
                $industry_id = $this->generateIndustryID($storeindustry, $inputdata['industry_name']);
                $oneindustry = R::load('industries', $storeindustry);
                $oneindustry->industry_id = $industry_id;
                R::store($oneindustry);
            }

            $objectresponse = [
                "industry_id" => $industry_id,
                "details" => R::exportAll($industry)
            ];

        }
        $status_code = 6000;
        $resultHandler = (new Statuses)->getstatus($status_code, $objectresponse);
        return $resultHandler;
    }

    public function listAllIndustries($page, $limit)
    {
        $offset = (($page - 1) * $limit);
        $all = R::findAll('industries');
        $totalindustries=(count($all));

        $all = R::findAll('industries');

        if (count($all)) {

            $industries = [];

            foreach ($all as $key) {
                
                unset($key['id']);
                unset($key['date_added']);
                unset($key['last_update']);
                
                array_push($industries, $key);
            }

            $status_code = 6000;
            $total_page=1;

            if(($totalindustries<$limit)>0){
                $totalpage=1;
            }else if(($totalindustries%$limit)!=0){
                $totalpage= ((int)($totalindustries/$limit))+1;
            }else{
                $total_page=($totalindustries/$limit);
            }

            $resultHandler = (new Statuses)->allAccountResponse($status_code, $industries, "", $page, $limit, $total_page);
            return $resultHandler;

        } elseif (!count($all) && $totalindustries>0) {
            $status_code = 6001;
            $resultHandler = (new Statuses)->allAccountResponse($status_code,0,"max pages exceeded",0);
            return $resultHandler;

        }else if(!count($all)){
            $status_code = 6001;
            $resultHandler = (new Statuses)->allAccountResponse($status_code,0,"no industries created yet",0);
            return $resultHandler;
        }
    }

    public function listOneIndustry($industry_id)
    {
        $oneindustry = R::findOne('industries', 'industry_id=?', [$industry_id]);
    
        if (count($oneindustry)>0) {

            $status_code = 6000;
            $resultHandler = (new Statuses)->getstatus($status_code, $oneindustry);
            
            return $resultHandler;

        }else{
            $status_code = 6001;
            $resultHandler = (new Statuses)->getstatus($status_code,"Industry not found");
            return $resultHandler;
        }

    }

}