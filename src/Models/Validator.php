<?php
namespace App\Models;

use App\Statuses\statuses as Statuses;
use App\Models\BanksModel as Banks;
use RedBeanPHP\R as R;

class Validator{

    public function authToken($auth_token, $user_id)
    {
        $errors = [];

        $tokencheck = R::findOne('authenticationtokens', 'user_id=? AND token=?', [$user_id, $auth_token]);
        
        if(count($tokencheck) > 0)
        {
            $datenow = date('Y-m-d H:i:s');
            $tokenexpiry = $tokencheck['expiry'];

            // echo $datenow > $tokenexpiry; die();

            if($datenow > $tokenexpiry)
            {
                $error = "Your token is expired, please login again to continue";
                array_push($errors, $error);
            }
        }
        else
        {
            $error = "You are not logged in";
            array_push($errors, $error);
        }
        
        $status_code = 6001;
        $resultHandler = (new Statuses)->getstatus($status_code, $errors);
        return $resultHandler;

    }

    public function UsersPayload($inputData)
    {
        $errors = [];

        foreach ($inputData as $key => $input){

            $name = str_replace(['_', '-'], ' ', $key);

            if($input == "" || $input == null){

                $error = $name . " must not be empty";
                array_push($errors, $error);

            }
        }
        $emailcheck = R::findOne('users', 'email=?', [$inputData['email']]);
        
        if (count($emailcheck) > 0)
        {
            $error = $inputData['email'] . " has already signed up, please use another email address";
            array_push($errors, $error);
        }
        
        $phonecheck = R::findOne('users', 'phone=?', [$inputData['phone']]);
        
        if (count($phonecheck) > 0)
        {
            $error = $inputData['phone'] . " has already signed up, please use another phone number";
            array_push($errors, $error);
        }
        
        $status_code = 6001;
        $resultHandler = (new Statuses)->getstatus($status_code, $errors);
        return $resultHandler;
        
    }

    public function administratorsPayload($inputData)
    {
        $errors = [];

        foreach ($inputData as $key => $input){

            $name = str_replace(['_', '-'], ' ', $key);

            if($input == "" || $input == null){

                $error = $name . " must not be empty";
                array_push($errors, $error);

            }
        }
        $emailcheck = R::findOne('administrators', 'email=?', [$inputData['email']]);
        
        if (count($emailcheck) > 0)
        {
            $error = $inputData['email'] . " has already signed up, please use another email address";
            array_push($errors, $error);
        }
        
        $phonecheck = R::findOne('administrators', 'phone=?', [$inputData['phone']]);
        
        if (count($phonecheck) > 0)
        {
            $error = $inputData['phone'] . " has already signed up, please use another phone number";
            array_push($errors, $error);
        }
        
        $status_code = 6001;
        $resultHandler = (new Statuses)->getstatus($status_code, $errors);
        return $resultHandler;
        
    }

    public function applicationPayload($job_id, $applicant_id)
    {
        $errors = [];

        $vacancycheck = R::findOne('vacancies', 'job_id=?', [$job_id]);
        if (count($vacancycheck) < 1)
        {
            $error = "Sorry this vacancy does not exist or has expired";
            array_push($errors, $error);
        }
        
        $applicationcheck = R::findOne('applications', 'job_id=? AND applicant_id=?', [$job_id, $applicant_id]);
        if (count($applicationcheck) > 0)
        {
            $error = "you have already applied for this position";
            array_push($errors, $error);
        }
        
        $status_code = 6001;
        $resultHandler = (new Statuses)->getstatus($status_code, $errors);
        return $resultHandler;
        
    }
    
    public function companiesPayload($inputData)
    {
        $errors = [];

        foreach ($inputData as $key => $input){

            $name = str_replace(['_', '-'], ' ', $key);

            if($input == "" || $input == null){

                $error = $name . " must not be empty";
                array_push($errors, $error);

            }
        }
        $emailcheck = R::findOne('companies', 'email=?', [$inputData['email']]);
        
        if (count($emailcheck) > 0)
        {
            $error = $inputData['email'] . " has already been signed up, please use another email address";
            array_push($errors, $error);
        }
        
        $phonecheck = R::findOne('companies', 'phone=?', [$inputData['phone']]);
        
        if (count($phonecheck) > 0)
        {
            $error = $inputData['phone'] . " has already been signed up, please use another phone number";
            array_push($errors, $error);
        }
        
        $status_code = 6001;
        $resultHandler = (new Statuses)->getstatus($status_code, $errors);
        return $resultHandler;
        
    }
    
    public function industriesPayload($inputData)
    {
        $errors = [];
        
        foreach ($inputData as $key => $input){
            
            $name = str_replace(['_', '-'], ' ', $key);
            
            if($input == "" || $input == null){
                
                $error = $name . " must not be empty";
                array_push($errors, $error);
                
            }
        }
        $namecheck = R::findOne('industries', 'industry_name=?', [$inputData['industry_name']]);
        
        if (count($namecheck) > 0)
        {
            $error = $inputData['industry_name'] . " has already been created with an ID of ".$namecheck['industry_id'];
            array_push($errors, $error);
        }
        
        $status_code = 6001;
        $resultHandler = (new Statuses)->getstatus($status_code, $errors);
        return $resultHandler;
        
    }

    public function vacanciesPayload($inputData)
    {
        $errors = [];
        
        foreach ($inputData as $key => $input){
            
            $name = str_replace(['_', '-'], ' ', $key);
            
            if($input == "" || $input == null){
                
                $error = $name . " must not be empty";
                array_push($errors, $error);
                
            }
        }
        
        $status_code = 6001;
        $resultHandler = (new Statuses)->getstatus($status_code, $errors);
        return $resultHandler;
        
    }
    
    public function confirmationPayload($inputData)
    {
        $errors = [];

        if(substr($inputData['user_id'], 0, 2) == 'AP'){
            $confirmationcheck = R::findOne('applicants', 'applicant_id=? AND account_confirmation=?', [ $inputData['user_id'], 1 ]);
        }
        else{
            $confirmationcheck = R::findOne('administrators', 'admin_id=? AND account_confirmation=?', [ $inputData['user_id'], 1 ]);
        }
        
        
        if (count($confirmationcheck) > 0){
            $error = "this account has already been confirmed";
            array_push($errors, $error);
        }
        
        if($inputData['password1'] !== $inputData['password2'])
        {
            $error = "Sorry, your password feilds must match, please try again";
            array_push($errors, $error);
            
        }
        
        $containsLetter  = preg_match('/[a-zA-Z]/',    $inputData['password1']);
        $containsDigit   = preg_match('/\d/',          $inputData['password1']);
        $containsSpecial = preg_match('/[^a-zA-Z\d]/', $inputData['password1']);
        
        $containsAll = $containsLetter && $containsDigit && $containsSpecial;
        
        if(!$containsLetter && !$containsDigit && !$containsSpecial)
        {
            $error = "Sorry, your password must contain at least one lowercase character, at least one uppercase character and at least one digit";
            array_push($errors, $error);
            
        }
        
        if(strlen($inputData['password1']) < 8)
        {
            $error = "Sorry, your password must be at least eight (8) digits long";
            array_push($errors, $error);
            
        }
        
        $confirmationcodecheck = R::findOne('confirmationcodes', 'user_id=? AND confirmation_code=?', [ $inputData['user_id'], $inputData['confirmation_code'] ]);
        
        if (count($confirmationcodecheck) < 1){
            $error = "this confirmation code does not exist";
            array_push($errors, $error);
        }
        
        $datenow = date("d-m-Y H:i:s");
       
        if ($confirmationcodecheck['expiry'] < $datenow){
            $error = "this confirmation code has expired, please try signing up again";
            array_push($errors, $error);
        }
        
        $status_code = 6001;
        $resultHandler = (new Statuses)->getstatus($status_code, $errors);
        return $resultHandler;
    }
    
    public function applicantAuthenticationPayload($inputData)
    {
        $errors = [];

        $emailcheck = R::findOne('applicants', 'email=?', [$inputData['email']]);
        
        if (count($emailcheck) < 1){
            $error = $inputData['email'] . " is not signed up, please follow the signup link below to get started";
            array_push($errors, $error);
        }
        
        $status_code = 6001;
        $resultHandler = (new Statuses)->getstatus($status_code, $errors);
        return $resultHandler;
    }

    public function adminAuthenticationPayload($inputData)
    {
        $errors = [];

        $emailcheck = R::findOne('administrators', 'email=?', [$inputData['email']]);
        
        if (count($emailcheck) < 1){
            $error = $inputData['email'] . " is not signed up, please follow the signup link below to get started";
            array_push($errors, $error);
        }
        
        $status_code = 6001;
        $resultHandler = (new Statuses)->getstatus($status_code, $errors);
        return $resultHandler;
    }
}