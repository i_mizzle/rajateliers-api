<?php
namespace App\Models;

use App\Statuses\Statuses;
use App\Models\Validator;
use App\Models\Mailer;
use RedBeanPHP\R as R;
use App\Models\VacanciesModel as Vacancies;
use App\Models\ApplicantsModel as Applicants;


class DashboardStatsModel
{
    public function getApplicantDashboardStats($applicant_id)
    {
        // STATISTICS:
        // percentage completion, 
        $totalfields = 16;
        $filledfields = 7;
        $doctypes = ['cv', 'letter', 'certification'];

        foreach($doctypes as $doctype){
            $doc = R::findOne('documents', 'document_type=? AND applicant_id=?', [$doctype, $applicant_id]);
            if(count($doc)){
                $filledfields++;
            }
        }
        
        $profile_photo = R::findOne('applicantimages', 'applicant_id=? AND image_role=?', [$applicant_id, 'profile']);
        if(count($profile_photo)){
            $filledfields++;
        }
        
        $fields = ['date_of_birth', 'career_level', 'career_type', 'highest_education', 'occupation', 'pay_range_lower'];
        
        $applicant_fields = R::findOne('applicants', 'applicant_id=?', [$applicant_id]);
        foreach($fields as $field){
            if($applicant_fields[$field] != NULL){
                $filledfields++;
            }
        }
        // number of vacancies for applicant, 
        $vacancies = (new Vacancies())->listAllVacanciesForApplicant(1, 10000, $applicant_id);
        if(count($vacancies['data'])){
            $uservacancies = count($vacancies['data']);
        }
        else{
            $uservacancies = 0;
        }

        $applications = R::findAll('applications', 'applicant_id=?', [$applicant_id]);
        if(count($applications)){
            $userapplications = count($applications);
        }
        else{
            $userapplications = 0;
        }

        $objectresponse = [
            "applications"=>$userapplications,
            "vacancies"=>$uservacancies,
            "percentage_completion"=>ceil(($filledfields/$totalfields)*100)
        ];

        $status_code = 6000;
        $resultHandler = (new Statuses)->getstatus($status_code, $objectresponse);
        return $resultHandler;
    
    }

    public function getAdminDashboardStats()
    {
        # code...
    }
}
