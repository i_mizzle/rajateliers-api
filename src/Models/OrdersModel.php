<?php
namespace App\Models;

use App\Statuses\Statuses;
use App\Models\Validator;
use App\Models\Mailer;
use RedBeanPHP\R as R;
use App\Models\ApplicantsModel as Applicants;
use App\Models\CompaniesModel as Companies;
use App\Models\VacanciesModel as Vacancies;

class ApplicationsModel
{
    private function generateApplicationID($applicant_id, $applicationindex)
    {
        $index = str_pad($applicationindex, 3, '0', STR_PAD_LEFT);
        $application_id = "APPL".$index;

        return $application_id;
    }

    public function createNewApplication($applicant_id, $job_id)
    {
        // echo $job_id; die();
        $application = R::dispense('applications');
        
        if($application){
            $application->id = "";
            $application->job_id = $job_id;
            $application->applicant_id = $applicant_id;
            $application->date_applied = R::isoDateTime();
            
            $storeapplication = R::store($application);

            if ($storeapplication) {
                $application_id = $this->generateApplicationID($applicant_id, $storeapplication);
                $oneapplication = R::load('applications', $storeapplication);
                $oneapplication->application_id = $application_id;
                R::store($oneapplication);
            }

            $objectresponse = [
                "application_id" => $application_id,
                "details" => R::exportAll($application)
            ];
        }

        $status_code = 6000;
        $resultHandler = (new Statuses)->getstatus($status_code, $objectresponse);
        return $resultHandler;
    }

    public function listAllApplications($page, $limit)
    {
        $offset = (($page - 1) * $limit);
        $all = R::findAll('applications');
        $totalapplications = (count($all));

        $all = R::findAll('applications');

        if (count($all)) {

            $applications = [];

            foreach ($all as $key) {

                $applicant = (new Applicants())->listOneApplicant($key['applicant_id']);
                $vacancy = (new Vacancies())->listOneVacancy($key['job_id']);

                $applicant_array = [
                    "applicant_id" => $applicant['data']['applicant_id'],
                    "applicant_name" => $applicant['data']['first_name']. " " .$applicant['data']['last_name'],
                    "email" => $applicant['data']['email'],
                    "location" => $applicant['data']['location']
                ];

                $vacancy_array = [
                    "job_id" => $vacancy['data']['job_id'],
                    "job_name" => $vacancy['data']['job_name'],
                    "company" => $vacancy['data']['company']
                ];
                
                $application = [
                    "application"=>$key['application_id'],
                    "date"=>$key['date_applied'],
                    "application_status"=>$key['status'],
                    "applicant"=>$applicant_array,
                    "job_details"=>$vacancy_array
                ];                
                array_push($applications, $application);
            }

            $status_code = 6000;
            $total_page=1;

            if(($totalapplications<$limit)>0){
                $totalpage=1;
            }else if(($totalapplications%$limit)!=0){
                $totalpage= ((int)($totalapplications/$limit))+1;
            }else{
                $total_page=($totalapplications/$limit);
            }

            $resultHandler = (new Statuses)->allAccountResponse($status_code, $applications, "", $page, $limit, $total_page);
            return $resultHandler;

        } elseif (!count($all) && $totalapplications>0) {
            $status_code = 6001;
            $resultHandler = (new Statuses)->allAccountResponse($status_code,0,"max pages exceeded",0);
            return $resultHandler;

        }else if(!count($all)){
            $status_code = 6001;
            $resultHandler = (new Statuses)->allAccountResponse($status_code,0,"no applications yet",0);
            return $resultHandler;
        }
    }

    public function listOneApplication($application_id)
    {
        $oneapplication = R::findOne('applications', 'application_id=?', [$application_id]);
    
        if (count($oneapplication)>0) {

            $applicant = (new Applicants())->listOneApplicant($oneapplication['applicant_id']);
            $vacancy = (new Vacancies())->listOneVacancy($oneapplication['job_id']);

            $applicant_array = [
                "applicant_id" => $applicant['data']['applicant_id'],
                "applicant_name" => $applicant['data']['first_name']. " " .$applicant['data']['last_name'],
                "email" => $applicant['data']['email'],
                "location" => $applicant['data']['location']
            ];

            $vacancy_array = [
                "job_id" => $vacancy['data']['job_id'],
                "job_name" => $vacancy['data']['job_name'],
                "industry" => $vacancy['data']['industry']
            ];
            
            $application = [
                "application"=>$oneapplication['application_id'],
                "date"=>$oneapplication['date_applied'],
                "application_status"=>$oneapplication['status'],
                "applicant"=>$applicant_array,
                "job_details"=>$vacancy_array
            ];  

            $status_code = 6000;
            $resultHandler = (new Statuses)->getstatus($status_code, $application);
            
            return $resultHandler;
        }
        else{
            $status_code = 6001;
            $resultHandler = (new Statuses)->getstatus($status_code,"Application not found");
            return $resultHandler;
        }

    }

    public function getApplicantApplications($page, $limit, $applicant_id)
    {
        $offset = (($page - 1) * $limit);
        $all = R::findAll('applications', 'applicant_id=?', [$applicant_id]);
        $totalapplications = (count($all));

        if (count($all)) {

            $applicant = (new Applicants())->listOneApplicant($applicant_id);            
            $applications=[];
            
            foreach ($all as $key) {

                $vacancy = (new Vacancies())->listOneVacancy($key['job_id']);

                $vacancy_array = [
                    "job_id" => $vacancy['data']['job_id'],
                    "job_name" => $vacancy['data']['job_name'],
                    "industry" => $vacancy['data']['industry']
                ];
                
                $application = [
                    "application_id"=>$key['application_id'],
                    "date"=>$key['date_applied'],
                    "application_status"=>$key['status'],
                    "job_details"=>$vacancy_array
                ];
                
                array_push($applications, $application);
            }
            
            $applicantwithapplications = [
                "applicant_id" => $applicant['data']['applicant_id'],
                "applicant_name" => $applicant['data']['first_name']. " " .$applicant['data']['last_name'],
                "email" => $applicant['data']['email'],
                "location" => $applicant['data']['location'],
                "applications" => $applications
            ];

            $status_code = 6000;
            $total_page=1;

            if(($totalapplications<$limit)>0){
                $totalpage=1;
            }else if(($totalapplications%$limit)!=0){
                $totalpage= ((int)($totalapplications/$limit))+1;
            }else{
                $total_page=($totalapplications/$limit);
            }

            $resultHandler = (new Statuses)->allAccountResponse($status_code, $applicantwithapplications, "", $page, $limit, $total_page);
            return $resultHandler;

        } elseif (!count($all) && $totalapplications>0) {
            $status_code = 6001;
            $resultHandler = (new Statuses)->allAccountResponse($status_code,0,"max pages exceeded",0);
            return $resultHandler;

        }else if(!count($all)){
            $status_code = 6001;
            $resultHandler = (new Statuses)->allAccountResponse($status_code,0,"no applications yet",0);
            return $resultHandler;
        }
   
    }

    public function getVacancyApplications($page, $limit, $job_id)
    {
        $offset = (($page - 1) * $limit);
        $all = R::findAll('applications', 'job_id=?', [$job_id]);
        $totalapplications = (count($all));

        if (count($all)) {

            $vacancy = (new Vacancies())->listOneVacancy($job_id);
            $applications=[];
            
            foreach ($all as $key) {
                
                $applicant = (new Applicants())->listOneApplicant($key['applicant_id']);            
                
                $application = [
                    "application_id"=>$key['application_id'],
                    "date"=>$key['date_applied'],
                    "application_status"=>$key['status']
                ];

                $applicant = [
                    "applicant_id" => $applicant['data']['applicant_id'],
                    "applicant_name" => $applicant['data']['first_name']. " " .$applicant['data']['last_name'],
                    "email" => $applicant['data']['email'],
                    "location" => $applicant['data']['location'],
                    "application" => $application
                ];

                
                
                array_push($applications, $applicant);
            }
            
            $vacancywithapplications = [
                "job_id" => $vacancy['data']['job_id'],
                "job_name" => $vacancy['data']['job_name'],
                "company" => $vacancy['data']['company'],
                "applications" => $applications
            ];

            $status_code = 6000;
            $total_page=1;

            if(($totalapplications<$limit)>0){
                $totalpage=1;
            }else if(($totalapplications%$limit)!=0){
                $totalpage= ((int)($totalapplications/$limit))+1;
            }else{
                $total_page=($totalapplications/$limit);
            }

            $resultHandler = (new Statuses)->allAccountResponse($status_code, $vacancywithapplications, "", $page, $limit, $total_page);
            return $resultHandler;

        } elseif (!count($all) && $totalapplications>0) {
            $status_code = 6001;
            $resultHandler = (new Statuses)->allAccountResponse($status_code,0,"max pages exceeded",0);
            return $resultHandler;

        }else if(!count($all)){
            $status_code = 6001;
            $resultHandler = (new Statuses)->allAccountResponse($status_code,0,"no applications yet",0);
            return $resultHandler;
        }
    }

}