<?php
namespace App\Models;

use App\Statuses\Statuses;
use App\Models\Validator;
use App\Models\Mailer;
use RedBeanPHP\R as R;

class CompaniesModel
{
    private function generateCompanyID($companyindex, $companyname)
    {
        $index = str_pad($companyindex, 3, '0', STR_PAD_LEFT);
        $name = strtoupper($companyname); 
        $company_id = "COM".substr($name,0,2).$index;

        return $company_id;
    }

    private function generateConfirmationCode($company_id)
    {
        $chars = "abcdefghijkmnopqrstuvwxyz023456789";
        srand((double)microtime()*1000000);
        $i = 0;
        $confirmationcode = '' ;

        while ($i <= 44) {
            $num = rand() % 45;
            $tmp = substr($chars, $num, 1);
            $confirmationcode = $confirmationcode . $tmp;
            $i++;
        }

        $account_confirmation_codes = R::dispense('confirmationcodes');

        if($account_confirmation_codes){
            $datenow = date("d-m-Y H:i:s");
            $expiry_date = date("Y-m-d H:i:s", strtotime($datenow)+259200);

            $account_confirmation_codes->user_id = $company_id;
            $account_confirmation_codes->confirmation_code = $confirmationcode;
            $account_confirmation_codes->expiry = $expiry_date;

            R::store($account_confirmation_codes);
        }

        return $confirmationcode;     
    }

    public function createNewCompany($inputdata)
    {
      $company = R::dispense('companies');

      if($company){
            $company->id = "";
            $company->company_name = $inputdata['company_name'];
            $company->contact_person = $inputdata['contact_person'];
            $company->phone = $inputdata['phone'];
            $company->location = $inputdata['location'];
            $company->email = $inputdata['email'];
            $company->industry = $inputdata['industry'];
            $company->about = $inputdata['about'];
            $company->confirmed = false;
            $company->date_signed_up = R::isoDateTime();

            $storecompany = R::store($company);

            if ($storecompany) {
                $company_id = $this->generateCompanyID($storecompany, $inputdata['company_name']);
                $onecompany = R::load('companies', $storecompany);
                $onecompany->company_id = $company_id;
                $onecompany->public_profile_url ="https://portal.jestrasolutions.com/company/".$company_id;

                R::store($onecompany);

                $confirmation_code = $this->generateConfirmationCode($company_id);

                $to = $inputdata['email'];
                $subject = "New account on Jestra Solutions Portal";
                $txt = "Hello
            
               A new account has been created on Jestra's Solutions Portal for the company " . $inputdata['company_name'] . ", please click the link below to confirm:
               https://jestrasolutions.com?user=" . $company_id . "&conf=" . $confirmation_code . "
               (copy and paste the link in your browser if it is not clickable). Please note that this link expires in 24 hrs.
               
               Kindly ignore this message if you did not initiate this request
               
               Jestra's Solutions Ltd";
                
                // $mailer = new Mailer;
                // $mailer->sendEmail($subject, $txt, $to);

            }

            $objectresponse = [
                "company_id" => $company_id,
                "details" => R::exportAll($company)
            ];

        }
        $status_code = 6000;
        $resultHandler = (new Statuses)->getstatus($status_code, $objectresponse);
        return $resultHandler;
    }

    public function listAllCompanies($page, $limit)
    {
        $offset = (($page - 1) * $limit);
        $all = R::findAll('companies', 'deleted=? AND confirmed=?', [0,1]);
        $totalcompanies=(count($all));

        $all = R::findAll('companies', 'deleted=? AND confirmed=? ORDER BY id LIMIT ?,?', [0, 1, $offset, $limit]);

        if (count($all)) {

            $companies = [];

            foreach ($all as $key) {
                
                unset($key['id']);
                unset($key['date_signed_up']);
                unset($key['confirmed']);
                unset($key['deleted']);
                unset($key['about']);
                unset($key['subscription_expiry']);
                unset($key['account_confirmation']);

                array_push($companies, $key);

            }

            $status_code = 6000;
            $total_page=1;

            if(($totalcompanies<$limit)>0){
                $totalpage=1;
            }else if(($totalcompanies%$limit)!=0){
                $totalpage= ((int)($totalcompanies/$limit))+1;
            }else{
                $total_page=($totalcompanies/$limit);
            }

            $resultHandler = (new Statuses)->allAccountResponse($status_code, $companies, "", $page, $limit, $total_page);
            return $resultHandler;

        } elseif (!count($all) && $totalcompanies>0) {
            $status_code = 6001;
            $resultHandler = (new Statuses)->allAccountResponse($status_code,0,"max pages exceeded",0);
            return $resultHandler;

        }else if(!count($all)){
            $status_code = 6001;
            $resultHandler = (new Statuses)->allAccountResponse($status_code,0,"no applicants available",0);
            return $resultHandler;
        }
    }

    public function searchForCompanies($filters)
    {

    }

    public function listOneCompany($company_id)
    {
        $onecompany = R::findOne('companies', 'company_id=?', [$company_id]);

        if (count($onecompany)>0) {

            $status_code = 6000;
            $resultHandler = (new Statuses)->getstatus($status_code, $onecompany);
            
            return $resultHandler;

        }else{
            $status_code = 6001;
            $resultHandler = (new Statuses)->getstatus($status_code,"Company not found");
            return $resultHandler;
        }

    }

    public function confirmCompanyAccount($company_id, $confirmation_code)
    {

        $company = R::findOne('companies', 'company_id=?', [$company_id]);

        $code_check = R::findOne('confirmationcodes', 'user_id=? AND confirmation_code=?', [$company_id, $confirmation_code]);

        if($code_check) {

            $companytoconfirm = R::load('companies', $company['id']);

            $companytoconfirm->confirmed = true;

            R::store($companytoconfirm);

            R::trash($code_check);

            $status_code = 6000;
            $resultHandler = (new Statuses)->getstatus($status_code, "Company Account Confirmed Successfully");
        }
        else{
            $status_code = 6001;
            $resultHandler = (new Statuses)->getstatus($status_code, "Invalid Confirmation Code");
        }

        return $resultHandler;

    }

}