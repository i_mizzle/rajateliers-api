<?php
namespace App\Models;

use App\Statuses\statuses as Statuses;
use App\Models\Validator;
use App\Models\Mailer;
use RedBeanPHP\R as R;
use App\Models\VacanciesModel as Vacancies;

class UsersModel
{
    private function generateUserID($userindex, $name, $location)
    {
        $index = str_pad($userindex, 5, '0', STR_PAD_LEFT);

        // $user_id = "U".substr($name,0,2).substr($location,0,2).$index;
        $user_id = strtoupper(substr($name,0,2)).strtoupper(substr($location,0,2)).$index;

        return $user_id;
    }

    private function generateConfirmationCode($user_id)
    {
        $chars = "abcdefghijkmnopqrstuvwxyz023456789";
        srand((double)microtime()*1000000);
        $i = 0;
        $confirmationcode = '' ;

        while ($i <= 44) {
            $num = rand() % 45;
            $tmp = substr($chars, $num, 1);
            $confirmationcode = $confirmationcode . $tmp;
            $i++;
        }

        $account_confirmation_codes = R::dispense('confirmationcodes');
        

        if($account_confirmation_codes){
            $datenow = date("d-m-Y H:i:s");
            $expiry_date = strtotime(date("Y-m-d H:i:s", strtotime($datenow)+86400));
            
            $account_confirmation_codes->id = "";
            $account_confirmation_codes->user_id = $user_id;
            $account_confirmation_codes->confirmation_code = $confirmationcode;
            $account_confirmation_codes->expiry = $expiry_date;
            
            R::store($account_confirmation_codes);
        }

        return $confirmationcode;     
    }

    private function createPassword($user_id, $password)
    {
        $hashed_password = password_hash($password, PASSWORD_DEFAULT);

        $userlogin = R::dispense('userlogin');

        if($userlogin){
            $applicantlogin->id = "";
            $userlogin->user_id = $user_id;
            $userlogin->password = $hashed_password;

            R::store($userlogin);
        }
    }

    public function createNewUser($inputdata)
    {
      $user = R::dispense('users');

      if($user){
            $user->id = "";
            $user->name = $inputdata['name'];
            $user->phone = $inputdata['phone'];
            $user->location = $inputdata['location'];
            $user->email = $inputdata['email'];
            $user->gender = $inputdata['gender'];
            $user->account_confirmation = 0;
            $user->signup_date = strtotime(R::isoDateTime());

            $storeuser = R::store($user);

            if ($storeuser) {
                $user_id = $this->generateUserID($storeuser, $inputdata['name'], $inputdata['location']);
                $oneuser = R::load('users', $storeuser);
                $oneuser->user_id = $user_id;

                R::store($oneuser);

                $confirmation_code = $this->generateConfirmationCode($user_id);
                $password = $this->createPassword($user_id, $inputdata['gender']);

                $to = $inputdata['email'];
                $subject = "New account on Raj Ateliers";
                $txt = "Hello " . $inputdata['name'] . "
            
               Your new account on Raj Ateliers has been created, please click the link below to confirm:
               https://rajateliers.com/confirmation/" . $user_id . "/" . $confirmation_code . "
               (copy and paste the link in your browser if it is not clickable). Please note that this link expires in 24 hrs.
               
               Kindly ignore this message if you did not initiate this request
               
               Raj Ateliers";
                
                // $mailer = new Mailer;
                // $mailer->sendEmail($subject, $txt, $to);

            }

            $objectresponse = [
                "user_id" => $user_id,
                "details" => R::exportAll($user)
            ];

        }
        $status_code = 6000;
        $resultHandler = (new Statuses)->getstatus($status_code, $objectresponse);
        return $resultHandler;
    }

    public function listAllUsers($page, $limit)
    {
        $offset = (($page - 1) * $limit);
        $all = R::findAll('users');
        $totalusers=(count($all));

        $all = R::findAll('users', 'ORDER BY id LIMIT ?,?', [$offset, $limit]);

        if (count($all)) {

            $users = [];

            foreach ($all as $key) {
                
                unset($key['id']);
                unset($key['date_created']);
                unset($key['confirmed']);
                unset($key['last_update']);

                array_push($users, $key);

            }

            $status_code = 6000;
            $total_page=1;

            if(($totalusers<$limit)>0){
                $totalpage=1;
            }else if(($totalusers%$limit)!=0){
                $totalpage= ((int)($totalusers/$limit))+1;
            }else{
                $total_page=($totalusers/$limit);
            }

            $resultHandler = (new Statuses)->allAccountResponse($status_code, $users, "", $page, $limit, $total_page);
            return $resultHandler;

        } elseif (!count($all) && $totalusers>0) {
            $status_code = 6001;
            $resultHandler = (new Statuses)->allAccountResponse($status_code,0,"max pages exceeded",0);
            return $resultHandler;

        }else if(!count($all)){
            $status_code = 6001;
            $resultHandler = (new Statuses)->allAccountResponse($status_code,0,"no users available",0);
            return $resultHandler;
        }
    }

    public function searchForUser($filters)
    {

    }

    public function listOneUser($user_id)
    {
        $oneuser = R::findOne('users', 'user_id=?', [$user_id]);

        if (count($oneuser)>0) {

            // $industries=[];
            // $applicantindustries = R::findAll('areasofinterest', 'user_id=?', [$applicant_id]);
            // foreach($applicantindustries as $applicantindustry){
            //     $singleindustry = R::findone('industries', 'industry_id=?', [$applicantindustry['industry_id']]);
            //     $industry = [
            //         "industry_id"=>$singleindustry['industry_id'],
            //         "industry_name"=>$singleindustry['industry_name']
            //     ];

            //     array_push($industries, $industry);
            // }

            // $applicantphoto = R::findone('applicantimages', 'applicant_id=?', [$applicant_id]);
            // if(count($applicantphoto)){
            //     $photo_url = $applicantphoto['image_url'];
            // }
            // else{
            //     $photo_url = "";
            // }

            $details = [
                "user_id"=>$oneuser['user_id'],
                "name"=>$oneuser['name'],
                "email"=>$oneuser['email'],
                "phone"=>$oneuser['phone'],
                "location"=>$oneuser['location'],
                "gender"=>$oneuser['gender']
            ];

            $status_code = 6000;
            $resultHandler = (new Statuses)->getstatus($status_code, $details);
            
            return $resultHandler;

        }else{
            $status_code = 6001;
            $resultHandler = (new Statuses)->getstatus($status_code,"User not found");
            return $resultHandler;
        }
    }    

    public function confirmUser($inputdata)
    {

        $applicant = R::findOne('users', 'user_id=?', [$inputdata['user_id']]);

        $code_check = R::findOne('confirmationcodes', 'user_id=? AND confirmation_code=?', [$inputdata['user_id'], $inputdata['confirmation_code']]);

        if($code_check) {

            $accounttoconfirm = R::load('applicants', $applicant['id']);

            $accounttoconfirm->account_confirmation = true;

            R::store($accounttoconfirm);

            R::trash($code_check);

            $this->createPassword($applicant['applicant_id'], $inputdata['password1']);

            $status_code = 6000;
            $resultHandler = (new Statuses)->getstatus($status_code, "Account Confirmed Successfully");
        }
        else{
            $status_code = 6001;
            $resultHandler = (new Statuses)->getstatus($status_code, "Invalid Confirmation Code");
        }

        return $resultHandler;

    }

    public function updateUserProfile($inputdata, $applicant_id)
    {

        $applicant = R::findOne('applicants', 'applicant_id=?', [ $applicant_id ]);
        
        if($applicant){
            $update = R::load('applicants', $applicant['id']);

            $update->first_name = $inputdata['first_name'];
            $update->last_name = $inputdata['last_name'];
            $update->phone = $inputdata['phone'];
            $update->location = $inputdata['location'];
            // $update->job_alerts = $inputdata['job_alerts'];
            $update->gender = $inputdata['gender'];
            $update->age = (int)$inputdata['age'];
            $update->occupation = $inputdata['occupation'];
            $update->career_level = $inputdata['career_level'];
            $update->career_type = $inputdata['job_type'];
            $update->highest_education = $inputdata['highest_education'];
            $update->pay_range_lower = (int)$inputdata['pay'];
            // $update->pay_range_higher = $inputdata['pay_range_higher'];
            $update->last_edit = R::isoDateTime();

            $updated = R::store($update);

            if($updated)
            {  
                $applicantindustries = $inputdata['industries'];

                foreach($applicantindustries as $applicantindustry){
                    $applicantindustrydb = R::findOne('areasofinterest', 'user_id=? AND industry_id=?', [$applicant_id, $applicantindustry]);
                    if(count($applicantindustrydb) < 1 ){
                        $this->storeApplicantAreaOfInterest($applicant_id, $applicantindustry);
                    }
                }
                $objectresponse = "Profile has been updated succesfully";                     
                $status_code = 6000;
                $resultHandler = (new Statuses)->getstatus($status_code, $objectresponse);
                return $resultHandler;
            }
            else
            {
                $objectresponse = "Update failed!";                     
                $status_code = 6001;
                $resultHandler = (new Statuses)->getstatus($status_code, $objectresponse);
                return $resultHandler;

            }
        }
    }
}