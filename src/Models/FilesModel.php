<?php
namespace App\Models;

use App\Statuses\Statuses;
use App\Models\Validator;
use App\Models\Mailer;
use RedBeanPHP\R as R;

class FilesModel
{
    
    public function saveImagePath($applicant_id, $path)
    {
        $applicantphoto = R::dispense('applicantimages');
        
        if($applicantphoto){
            $applicantphoto->id = "";
            $applicantphoto->applicant_id = $applicant_id;
            $applicantphoto->image_url = $path;
            $applicantphoto->date_added = R::isoDateTime();

            $storeapplicantphoto = R::store($applicantphoto);

            $objectresponse = "Photo uploaded sucessfully";
        }
        $status_code = 6000;
        $resultHandler = (new Statuses)->getstatus($status_code, $objectresponse);
        return $resultHandler;
    }

    public function saveDocumentPath($applicant_id, $path, $file_type, $doc_type, $doc_name)
    {
        $applicantdoc = R::dispense('documents');
        
        if($applicantdoc){
            $applicantdoc->id = "";
            $applicantdoc->applicant_id = $applicant_id;
            $applicantdoc->document_url = $path;
            $applicantdoc->file_type = $file_type;
            $applicantdoc->document_type = $doc_type;
            $applicantdoc->document_name = $doc_name;
            $applicantdoc->date_uploaded = R::isoDateTime();

            $storeapplicantdoc = R::store($applicantdoc);

            $objectresponse = "Document uploaded sucessfully";
        }
        $status_code = 6000;
        $resultHandler = (new Statuses)->getstatus($status_code, $objectresponse);
        return $resultHandler;
    }

    public function listAllApplicantDocuments($applicant_id)
    {
        $all = R::findAll('documents', 'applicant_id=? AND deleted=?', [$applicant_id, 0]);

        if (count($all)) {

            $applicant_docs = [];

            foreach ($all as $applicant_doc) {
                
                unset($applicant_doc['id']);
                unset($applicant_doc['deleted']);
                array_push($applicant_docs, $applicant_doc);

            }

            $status_code = 6000;
            
            $resultHandler = (new Statuses)->getstatus($status_code, $applicant_docs);
            
            return $resultHandler;

        }else{
            $status_code = 6001;
            
            $resultHandler = (new Statuses)->getstatus($status_code, "No documents uploaded yet, you should upload at least a CV before you can apply for positions");
            
            return $resultHandler;
        }
    }
}
