<?php
namespace App\Models;

use App\Statuses\Statuses;
use App\Models\Validator;
use App\Models\Mailer;
use RedBeanPHP\R as R;

class VacanciesModel
{
    private function generateVacancyID($vacancyindex, $companyid)
    {
        $index = str_pad($vacancyindex, 2, '0', STR_PAD_LEFT);
        $company_id = strtoupper($companyid); 
        $vacancy_id = $company_id."VAC".$index;

        return $vacancy_id;
    }

    public function createNewVacancy($inputdata)
    {
        $vacancy = R::dispense('vacancies');

        $publish_date = date('Y-m-d H:i:s');
        $time = $inputdata['time']*86400;
        $expiry_date = date("Y-m-d H:i:s", strtotime($publish_date)+$time);
        
        $company = R::findOne('companies', 'company_id=?', [$inputdata['company_id']]);
        $company_name = $company['company_name'];    
        
        // echo $company_name; die();

        if($vacancy){
            $vacancy->id = "";
            $vacancy->job_name = $inputdata['job_name'];
            $vacancy->name_sounds_like = metaphone($inputdata['job_name']);
            $vacancy->company_id = $inputdata['company_id'];
            $vacancy->company_sounds_like = metaphone($company_name);
            $vacancy->job_description = $inputdata['job_description'];
            $vacancy->date_posted = R::isoDateTime();
            $vacancy->expiry_date = $expiry_date;
            $vacancy->published = $inputdata['published'];
            $vacancy->salary = $inputdata['salary'];
            $vacancy->job_location = $inputdata['job_location'];
            $vacancy->requirements = $inputdata['requirements'];
            $vacancy->job_type = $inputdata['job_type'];
            $vacancy->job_level = $inputdata['job_level'];
            $vacancy->minimum_qualification = $inputdata['minimum_qualification'];
            $vacancy->job_experience = $inputdata['job_experience'];

            $storevacancy = R::store($vacancy);

            if ($storevacancy) {
                $job_id = $this->generateVacancyID($storevacancy, $inputdata['company_id']);
                $onevacancy = R::load('vacancies', $storevacancy);
                $onevacancy->job_id = $job_id;
                R::store($onevacancy);
            }

            $objectresponse = [
                "job_id" => $job_id,
                "details" => R::exportAll($vacancy)
            ];

        }
        $status_code = 6000;
        $resultHandler = (new Statuses)->getstatus($status_code, $objectresponse);
        return $resultHandler;
    }

    public function listAllVacancies($page, $limit)
    {
        $offset = (($page - 1) * $limit);
        $all = R::findAll('vacancies');
        $totalvacancies=(count($all));

        if (count($all)) {

            $vacancies = [];

            foreach ($all as $key) {
                
                $company = R::findOne('companies', 'company_id=?', [$key['company_id']]); 
                $industry =  R::findOne('industries', 'industry_id=?', [$company['industry']]); 
                
                $vacancy = [
                    "job_id"=>$key['job_id'],
                    "job_name"=>$key['job_name'],
                    "industry"=>$industry['industry_name'],
                    "salary"=>(int)$key['salary'],
                    "publish_status"=>$key['published'],
                ];                
                array_push($vacancies, $vacancy);
            }
                          

            $status_code = 6000;
            $total_page=1;

            if(($totalvacancies<$limit)>0){
                $totalpage=1;
            }else if(($totalvacancies%$limit)!=0){
                $totalpage= ((int)($totalvacancies/$limit))+1;
            }else{
                $total_page=($totalvacancies/$limit);
            }

            $resultHandler = (new Statuses)->allAccountResponse($status_code, $vacancies, "", $page, $limit, $total_page);
            return $resultHandler;

        } elseif (!count($all) && $totalvacancies>0) {
            $status_code = 6001;
            $resultHandler = (new Statuses)->allAccountResponse($status_code,0,"max pages exceeded",0);
            return $resultHandler;

        }else if(!count($all)){
            $status_code = 6001;
            $resultHandler = (new Statuses)->allAccountResponse($status_code,0,"no vacancies created yet",0);
            return $resultHandler;
        }
    }

    public function listAllVacanciesForApplicant($page, $limit, $applicant_id)
    {
        // get all industries in applicant's aoi
        $applicantindustries = R::findAll('areasofinterest', 'user_id=?', [$applicant_id]);
        $totalvacancies = 0;

        // PRINT_R($applicantindustries);die();

        if(count($applicantindustries)){
            // foreach industry, get all vacancies aand add to the results array
            
            $vacancies = [];

            foreach($applicantindustries as $applicantindustry){

                $applicantvacancies = R::findAll('vacancies', 'industry_id=?', [$applicantindustry['industry_id']]);
                if(count($applicantvacancies)){
                    $industry =  R::findOne('industries', 'industry_id=?', [$applicantindustry['industry_id']]); 

                    foreach($applicantvacancies as $applicantvacancy){
                    $vacancy = [
                        "job_id"=>$applicantvacancy['job_id'],
                        "job_name"=>$applicantvacancy['job_name'],
                        "industry"=>$industry['industry_name'],
                        "salary"=>(int)$applicantvacancy['salary'],
                        "publish_status"=>$applicantvacancy['published'],
                        "application_deadline"=>$applicantvacancy['expiry_date'],
                    ];                
                    array_push($vacancies, $vacancy);
                }

                $totalvacancies+count($applicantvacancies);
                }            
            
                $status_code = 6000;
                $total_page=1;
    
                if(($totalvacancies<$limit)>0){
                    $totalpage=1;
                }else if(($totalvacancies%$limit)!=0){
                    $totalpage= ((int)($totalvacancies/$limit))+1;
                }else{
                    $total_page=($totalvacancies/$limit);
                }
    
                $resultHandler = (new Statuses)->allAccountResponse($status_code, $vacancies, "", $page, $limit, $total_page);
                return $resultHandler;
    
            }
        } 
        elseif (!count($applicantindustries) && $totalvacancies>0) {
            $status_code = 6001;
            $resultHandler = (new Statuses)->allAccountResponse($status_code,0,"max pages exceeded",0);
            return $resultHandler;

        }
        else if(!count($applicantindustries)){
            $status_code = 6001;
            $resultHandler = (new Statuses)->allAccountResponse($status_code,0,"Sorry, you have not selected your areas of interest yet.",0);
            return $resultHandler;
        }
    }

    public function listAllVacanciesForCompany($page, $limit, $company_id)
    {
        $offset = (($page - 1) * $limit);
        $all = R::findAll('vacancies', 'company=?', [$company_id]);
        $totalvacancies=(count($all));

        if (count($all)) {

            $vacancies = [];

            foreach ($all as $key) {
                
                $company = R::findOne('companies', 'company_id=?', [$key['company_id']]); 
                $industry =  R::findOne('industries', 'industry_id=?', [$company['industry']]); 
                
                $vacancy = [
                    "job_id"=>$key['job_id'],
                    "job_name"=>$key['job_name'],
                    "industry"=>$industry['industry_name'],
                    "salary"=>(int)$key['salary'],
                    "publish_status"=>$key['published'],
                ];                
                array_push($vacancies, $vacancy);
            }
                          

             $status_code = 6000;
            $total_page=1;

            if(($totalvacancies<$limit)>0){
                $totalpage=1;
            }else if(($totalvacancies%$limit)!=0){
                $totalpage= ((int)($totalvacancies/$limit))+1;
            }else{
                $total_page=($totalvacancies/$limit);
            }

            $resultHandler = (new Statuses)->allAccountResponse($status_code, $vacancies, "", $page, $limit, $total_page);
            return $resultHandler;

        } elseif (!count($all) && $totalvacancies>0) {
            $status_code = 6001;
            $resultHandler = (new Statuses)->allAccountResponse($status_code,0,"max pages exceeded",0);
            return $resultHandler;

        }else if(!count($all)){
            $status_code = 6001;
            $resultHandler = (new Statuses)->allAccountResponse($status_code,0,"no vacancies created yet",0);
            return $resultHandler;
        }
    }

    public function listOneVacancy($job_id)
    {
        $onevacancy = R::findOne('vacancies', 'job_id=?', [$job_id]);
        
        if (count($onevacancy)>0) {
            $company = R::findOne('companies', 'company_id=?', [$onevacancy['company_id']]);        
            $industry =  R::findOne('industries', 'industry_id=?', [$company['industry']]);

            $vacancy = [
                "job_id"=>$onevacancy['job_id'],
                "job_name"=>$onevacancy['job_name'],
                "industry"=>$industry['industry_name'],
                "job_description"=>$onevacancy['job_description'],
                "date_posted"=>$onevacancy['date_posted'],
                "expiry_date"=>$onevacancy['expiry_date'],
                "salary"=>(int)$onevacancy['salary'],
                "location"=>$onevacancy['job_location'],
                "requirements"=>$onevacancy['requirements'],
                "job_type"=>$onevacancy['job_type'],
                "job_level"=>$onevacancy['job_level'],
                "minimum_qualification"=>$onevacancy['minimum_qualification'],
                "experience"=>(int)$onevacancy['job_experience'],
                "publish_status"=>$onevacancy['published'],
            ];

            $status_code = 6000;
            $resultHandler = (new Statuses)->getstatus($status_code, $vacancy);
            
            return $resultHandler;

        }else{
            $status_code = 6001;
            $resultHandler = (new Statuses)->getstatus($status_code,"vacancy not found");
            return $resultHandler;
        }

    }

    public function searchForVacancy($search_term)
    {
        $search = R::findAll("SELECT * FROM vacancies WHERE name_sounds_like like '%$search_term%'");
        
        $resultarray = [];
            if (count($search)>0) {
                
                foreach($search as $search_result){
                    $vacancy = [
                        "job_id"=>$search_result['job_id'],
                        "name"=>$search_result['job_name']
                    ];
                    array_push($resultarray, $vacancy);
                }
    
                $status_code = 6000;
                $resultHandler = (new Statuses)->getstatus($status_code, $resultarray);
                
                return $resultHandler;
    
            }else{
                $status_code = 6001;
                $resultHandler = (new Statuses)->getstatus($status_code,"no results found for your search");
                return $resultHandler;
            }
    
    }

    public function deleteOneVacancy($job_id)
    {
        $vacancy = R::findOne('vacancies', 'job_id=?', [ $job_id ]);
        // print_r($vacancy); die();
        if($vacancy){
            $delete = R::load('vacancies', $vacancy['id']);

            $delete->deleted = 1;
            
            R::store($delete);

            $objectresponse = "Vacancy has been deleted succesfully";                     
            $status_code = 6000;
            $resultHandler = (new Statuses)->getstatus($status_code, $objectresponse);
            return $resultHandler;
            
        }   
        else{
            $objectresponse = "Vacancy not found";                     
            $status_code = 6000;
            $resultHandler = (new Statuses)->getstatus($status_code, $objectresponse);
            return $resultHandler;
        } 
        
    }

}