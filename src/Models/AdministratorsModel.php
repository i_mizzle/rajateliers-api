<?php
namespace App\Models;

use App\Statuses\Statuses;
use App\Models\Validator;
use App\Models\Mailer;
use RedBeanPHP\R as R;

class AdministratorsModel
{
    private function generateAdministratorID($administratorindex, $firstname, $lastname)
    {
        $index = str_pad($administratorindex, 2, '0', STR_PAD_LEFT);

        $user_id = "AD".substr($firstname,0,1).substr($lastname,0,1).$index;

        return $user_id;
    }

    private function generateConfirmationCode($user_id)
    {
        $chars = "abcdefghijkmnopqrstuvwxyz023456789";
        srand((double)microtime()*1000000);
        $i = 0;
        $confirmationcode = '' ;

        while ($i <= 44) {
            $num = rand() % 45;
            $tmp = substr($chars, $num, 1);
            $confirmationcode = $confirmationcode . $tmp;
            $i++;
        }

        $account_confirmation_codes = R::dispense('confirmationcodes');
        

        if($account_confirmation_codes){
            $datenow = date("d-m-Y H:i:s");
            $expiry_date = strtotime(date("Y-m-d H:i:s", strtotime($datenow)+86400));
            
            $account_confirmation_codes->id = "";
            $account_confirmation_codes->user_id = $user_id;
            $account_confirmation_codes->confirmation_code = $confirmationcode;
            $account_confirmation_codes->expiry = $expiry_date;
            
            R::store($account_confirmation_codes);
        }

        return $confirmationcode;     
    }

    private function createPassword($user_id, $password)
    {
        $hashed_password = password_hash($password, PASSWORD_DEFAULT);

        $administratorlogin = R::dispense('adminlogin');

        if($administratorlogin){
            $administratorlogin->id = "";
            $administratorlogin->admin_id = $user_id;
            $administratorlogin->password = $hashed_password;

            R::store($administratorlogin);
        }
    }

    public function createNewAdministrator($inputdata)
    {
      $administrator = R::dispense('administrators');

      if($administrator){
            $administrator->id = "";
            $administrator->first_name = $inputdata['first_name'];
            $administrator->last_name = $inputdata['last_name'];
            $administrator->phone = $inputdata['phone'];
            $administrator->email = $inputdata['email'];
            $administrator->gender = $inputdata['gender'];
            $administrator->account_confirmation = 0;
            $administrator->signup_date = strtotime(R::isoDateTime());
            
            $storeadministrator = R::store($administrator);

            if ($storeadministrator) {
                $administrator_id = $this->generateAdministratorID($storeadministrator, $inputdata['first_name'], $inputdata['last_name']);
                $oneadministrator = R::load('administrators', $storeadministrator);
                $oneadministrator->admin_id = $administrator_id;

                R::store($oneadministrator);

                $confirmation_code = $this->generateConfirmationCode($administrator_id);

                $to = $inputdata['email'];
                $subject = "New adminstrator account on Rajateliers";
                $txt = "Hello " . $inputdata['first_name'] . "
            
               A new administrator account has been created for you on Rajateliers Administrators Portal, please click the link below to confirm:
               https://jestrasolutions.com?user=" . $administrator_id . "&conf=" . $confirmation_code . "
               (copy and paste the link in your browser if it is not clickable). Please note that this link expires in 72 hrs.
                              
               Rajeteliers";
                
                // $mailer = new Mailer;
                // $mailer->sendEmail($subject, $txt, $to);

            }

            $objectresponse = [
                "user_id" => $administrator_id,
                "details" => R::exportAll($administrator)
            ];

        }
        $status_code = 6000;
        $resultHandler = (new Statuses)->getstatus($status_code, $objectresponse);
        return $resultHandler;
    }

    public function listAllAdministrators($page, $limit)
    {
        $offset = (($page - 1) * $limit);
        $all = R::findAll('administrators');
        $totaladministrators=(count($all));

        $all = R::findAll('administrators', 'account_confirmation=? ORDER BY id LIMIT ?,?', [1, $offset, $limit]);

        if (count($all)) {

            $administrators = [];

            foreach ($all as $key) {
                
                unset($key['id']);
                unset($key['created_at']);
                unset($key['account_confirmation']);
                unset($key['last_update']);

                array_push($administrators, $key);

            }

            $status_code = 6000;
            $total_page=1;

            if(($totaladministrators<$limit)>0){
                $totalpage=1;
            }else if(($totaladministrators%$limit)!=0){
                $totalpage= ((int)($totaladministrators/$limit))+1;
            }else{
                $total_page=($totaladministrators/$limit);
            }

            $resultHandler = (new Statuses)->allAccountResponse($status_code, $administrators, "", $page, $limit, $total_page);
            return $resultHandler;

        } elseif (!count($all) && $totaladministrators>0) {
            $status_code = 6001;
            $resultHandler = (new Statuses)->allAccountResponse($status_code,0,"max pages exceeded",0);
            return $resultHandler;

        }else if(!count($all)){
            $status_code = 6001;
            $resultHandler = (new Statuses)->allAccountResponse($status_code,0,"no administrators available",0);
            return $resultHandler;
        }
   }

    public function listOneAdministrator($administrator_id)
    {
        $oneadministrator = R::findOne('administrators', 'admin_id=?', [$administrator_id]);

        if (count($oneadministrator)>0) {

            $status_code = 6000;
            $resultHandler = (new Statuses)->getstatus($status_code, $oneadministrator);
            
            return $resultHandler;

        }else{
            $status_code = 6001;
            $resultHandler = (new Statuses)->getstatus($status_code,"Administrator not found");
            return $resultHandler;
        }

    }

    public function confirmAdministrator($inputdata)
    {

        $administrator = R::findOne('administrators', 'admin_id=?', [$inputdata['user_id']]);

        $code_check = R::findOne('confirmationcodes', 'user_id=? AND confirmation_code=?', [$inputdata['user_id'], $inputdata['confirmation_code']]);

        if($code_check) {

            $accounttoconfirm = R::load('administrators', $administrator['id']);

            $accounttoconfirm->account_confirmation = 1;

            R::store($accounttoconfirm);

            R::trash($code_check);

            $this->createPassword($administrator['admin_id'], $inputdata['password1']);

            $status_code = 6000;
            $resultHandler = (new Statuses)->getstatus($status_code, "Account Confirmed Successfully");
        }
        else{
            $status_code = 6001;
            $resultHandler = (new Statuses)->getstatus($status_code, "Invalid Confirmation Code");
        }

        return $resultHandler;

    }

}